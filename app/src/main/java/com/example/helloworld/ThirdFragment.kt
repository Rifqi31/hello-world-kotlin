package com.example.helloworld

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.longtailvideo.jwplayer.JWPlayerView
import com.longtailvideo.jwplayer.configuration.PlayerConfig
import com.longtailvideo.jwplayer.media.playlists.PlaylistItem


class ThirdFragment : Fragment() {

    // property
    lateinit var mPlayerView: JWPlayerView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_third, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mPlayerView = view.findViewById(R.id.jwplayer)
        val playlistItem = PlaylistItem.Builder()
            .file("https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8")
            .title("Sintel")
            .description("A woman who disappointed her self to kill a dragon ")
            .image("https://upload.wikimedia.org/wikipedia/commons/thumb/8/8f/Sintel_poster.jpg/640px-Sintel_poster.jpg")
            .build()
        val playlist: MutableList<PlaylistItem> = ArrayList()
        playlist.add(playlistItem)
        val config = PlayerConfig.Builder()
            .playlist(playlist)
            .build()
        mPlayerView.setup(config)

        // button for navigate to another fragment
        view.findViewById<Button>(R.id.button_back).setOnClickListener{
            // back to navigate first navigate
            findNavController().navigate(R.id.action_thirdFragment_to_FirstFragment)
        }
    }

    // play
    override fun onStart() {
        super.onStart()
        mPlayerView!!.onStart()
    }

    // resume
    override fun onResume() {
        super.onResume()
        mPlayerView.onResume()
    }

    // pause
    override fun onPause() {
        super.onPause()
        mPlayerView.onPause()
    }

    // top
    override fun onStop() {
        super.onStop()
        mPlayerView.onStop()
    }

    // destroy
    override fun onDestroy() {
        super.onDestroy()
        mPlayerView.onDestroy()
    }

}