package com.example.helloworld

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.fragment.findNavController

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FirstFragment : Fragment() {

    // count function
    private fun countMe(view: View) {
        // get the text view
        val showCountTextView = view.findViewById<TextView>(R.id.textview_first)
        // get the value of the text view
        val countString = showCountTextView.text.toString()
        // convert value to a number and increment it
        var count = countString.toInt()
        count++
        // display the new value in the text view
        showCountTextView.text = count.toString()
    }

    // on create view
    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_first, container, false)
    }

    // on view created
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // button count
        view.findViewById<Button>(R.id.button_count).setOnClickListener {
            // findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment)
            // define variable to contain TextView
            val showCountTextView = view.findViewById<TextView>(R.id.textview_first)
            // convert from string into int
            val currentCount = showCountTextView.text.toString().toInt()
            // define variable action with contain count function
            val action = FirstFragmentDirections.actionFirstFragmentToSecondFragment(currentCount)
            // find navigation controller
            findNavController().navigate(action)
        }

        // button toast
        view.findViewById<Button>(R.id.button_toast).setOnClickListener {
            // create a toast with some text, to appear for short time
            val myToast = Toast.makeText(context, "Hello Toast!", Toast.LENGTH_SHORT)
            // show the toast
            myToast.show()
        }

        // button random
        view.findViewById<Button>(R.id.button_random).setOnClickListener{
            // call function
            countMe(view)
        }

        // button video
        view.findViewById<Button>(R.id.button_video).setOnClickListener{
            // navigate to thirdFragment
            findNavController().navigate(R.id.action_FirstFragment_to_thirdFragment)
        }
    }
}
